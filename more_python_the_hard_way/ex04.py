#!/usr/bin/python

import sys
import argparse

print ("this script has the name %s") % (sys.argv[0])

parser=argparse.ArgumentParser(
    description='This is my example help menu.')
parser.add_argument('--foo', help='FOO!') # type=int, default=42,
parser.add_argument('bar', help='BAR!') # nargs='*', default=[1, 2, 3],
args=parser.parse_args()