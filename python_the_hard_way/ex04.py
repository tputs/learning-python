#!/usr/bin/env python3
#
# define amount of cars
cars = 100
# define space in each car
space_in_a_car = 4.0
# define amount of drivers
drivers = 30
# define amount of passengers
passengers = 90
# define cars not driven as cars minus drivers
cars_not_driven = cars - drivers
# define cars driven as equal to amount of drivers
cars_driven = drivers
# define carpool capacity as cars driven multiplied by space in each car
carpool_capacity = cars_driven * space_in_a_car
# define average passengers per car as passengers divided by cars driven
average_passengers_per_car = passengers / cars_driven
#
print("There are", cars, "cars available.")
#
print("There are only", drivers, "drivers available.")
#
print("There will be", cars_not_driven, "empty cars today.")
#
print("We can transport", carpool_capacity, "people today.")
#
print("We have", passengers, "to carpool today.")
#
print("We need to put about",average_passengers_per_car,"in each car.")
