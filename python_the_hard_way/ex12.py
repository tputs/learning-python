#!/usr/bin/env python3
# get some data from the user using input function and assign a variable name
age = input("How old are you? ") #
height = input("How tall are you? ")
weight = input("How much do you weigh? ")

print(f"So, you're {age} old, {height} tall and {weight} heavy.") # call those variables in a print statement.
 
# study drills, use pydoc to read about open, file, os, and sys.
#
# open; opens file objects, the preferred way. 
# file; open a file in various modes or create a file
# os; a thing that if imported makes python more portable. i'd like to understand this better.
# sys; i have no idea