#!/usr/bin/env python3
from sys import argv

script, filename = argv
print(argv)
txt = open(filename) #file name is now a string

print(f"Here's your filename {filename}:")
print(txt.read())

print("Type the filename again:")
file_again = input("> ")

txt_again = open(file_again)

print(txt_again.read())