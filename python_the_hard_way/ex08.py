#!/usr/bin/env python3
formatter = "{} {} {} {}"

print(formatter.format(1, 2, 3, 4))
print(formatter.format("one", "two", "three", "four"))
print(formatter.format(True, False, False, True))
print(formatter.format(formatter, formatter, formatter, formatter))
print(formatter.format(
    "All our times have come",
    "Here but now they're gone",
    "Seasons don't fear the reaper",
    "Nor do the wind, the sun or the rain, we can be like they are"
))
