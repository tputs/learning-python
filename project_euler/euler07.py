#!/usr/bin/env python3
###
# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
# What is the 10 001st prime number?
###

i = 1
prime_index = 0

def is_prime(num):
    if num <= 3:
        return num > 1
    elif num % 2 == 0 or num % 3 == 0:
        return False
    else:
        for x in range(2,num):    
            if num % x == 0:
                return False
    return True


while prime_index < 10001:
    i += 1
    if is_prime(i):
        prime_index += 1
print(prime_index, i)