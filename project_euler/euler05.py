#!/usr/bin/env python3
###
# Euler Problem 5: Smallest multiple
# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
###

num = 20 # i'll use 20 to iterate on because it is the largest common divisor

# a function that returns true if num is evenly divisible by 2-20, skip 1 because reasons
def div_by_range(num):
    for i in range(2, 21):
        if num % i != 0:
            return False
    return True    

# a loop that adds 1 every time my iterable is not true and prints once it reaches the lowest evenly divisble number
while True:
    if div_by_range(num):
        break
    else:
        num += 1
print(num)

# its kinda slow. i want faster.