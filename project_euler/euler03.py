#!/usr/bin/env python3
###
# Project Euler problem 3: largest prime factor https://projecteuler.net/problem=3
# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of 600851475143?
###

from math import sqrt
# import math as sandwich -- renaming a library

i = 1                           # something to iterate on
lpf = 0                         # largest prime factor
interested = 600851475143       # number we're interested in finding the largest prime factor of


def is_prime(num):
    if num <= 3:
        return num > 1
    elif num % 2 == 0 or num % 3 == 0:
        return False
    else:
        for x in range(2,num):    
            if num % x == 0:
                return False
    return True


def is_factor(pot_fac, num_chk):
    if num_chk % pot_fac == 0:
        return True
    return False


while i < sqrt(interested):
    i += 1
    if is_factor(i, interested):
        if is_prime(i):
            lpf = i

print(f"{lpf} is the largest prime factor of {interested}.")