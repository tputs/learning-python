#!/usr/bin/env python3
###
# Project Euler problem 4: largest palindrome product https://projecteuler.net/problem=4
# A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
# Find the largest palindrome made from the product of two 3-digit numbers.
###

a = list(range(999, 100, -1))
b = list(range(999, 100, -1))
largestpal = 0

def is_palindrome(num):
    if str(num) == str(num)[::-1]:
        return True
    else:
        return False


for i in a:
    for j in b:
        product = i * j
        if is_palindrome(product):
            if product > largestpal:
                largestpal = product
                
print(largestpal)


# homework 2
# get that answer, its gonna be wrong. find out why. try a different way. change how i generate multiplication. get a big palindrome first.


# homework 1
# hint str(listo[1]) -- remove the brackets aka list splice
# think about scope when working within my function. scoping may let me make choices that i might not think of with bash.