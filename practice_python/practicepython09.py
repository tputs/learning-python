#!/usr/bin/env python3
#
# Generate a random number between 1 and 9 (including 1 and 9). Ask the user to guess the number,
# then tell them whether they guessed too low, too high, or exactly right. 
# (Hint: remember to use the user input lessons from the very first exercise)
#
# Extras:
#    Keep the game going until the user types “exit”
#    Keep track of how many guesses the user has taken, and when the game ends, print this out.

import random

y = random.randint(1, 9) # get random for a random integer
x = int(input("Guess a number:\n> "))

if (x > y):
    print("Too high.")
elif (x < y):
    print("Too low.")
else:
    print("That is exactly right!")
