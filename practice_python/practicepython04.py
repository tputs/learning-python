#!/usr/bin/env python3
###
# Practice Python 4: divisors
# Create a program that asks the user for a number and then prints out a list of all the divisors of that number.
###

num = int(input("Give a number:  "))
listo = []

for i in range(1,num+1):
    if num % i == 0:
        listo.append(i)
print(listo)