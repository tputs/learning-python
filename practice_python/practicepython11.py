#!/usr/bin/env python3
#
# Original: Ask the user for a number and determine whether the number is prime or not.
# (For those who have forgotten, a prime number is a number that has no divisors.).
# You can (and should!) use your answer to Exercise 4 to help you. Take this opportunity to practice using functions, described below.

def get_num(prompt):
    return int(input(prompt))


def is_prime(num):
    if num <= 3:
        return num > 1 # rule out easy numbers
    elif num % 2 == 0 or num % 3 == 0:
        return False # rule out multiples of 3 or 5
    else:
        for x in range(2,num):    
            if num % x == 0:
                return False # rule out divisible by given number
    return True


def print_is_prime(num):
    if is_prime(num):
        descriptor = ""
    else:
        descriptor = "not "
    print(num," is ", descriptor, "prime.", sep = "", end = "\n\n")


while True:
    print_is_prime(get_num("Enter a number. CTRL + C to exit.\n\n>>> "))


# follow up homework: how to do every other number.

# before making any changes, tested to 25 and that is when it first returned incorrect. then used for loop and that works beyond 50.
# i then looked at the sample solution and copied some code to make it easier to test.


# Goal2: write a function, pass the function an int, it will return true if the number is 5 and it will return false for any other number.
# don't print, just return. use print to proof, though.

# then

# write a function where i pass it two strings
# print on two lines
# return value will return true if they are the same string, false if they don't match.

# def is_num_five():
#     if (num == 5):
#         return True
#         # print("True")
#     else:
#         return False
#         # print("False")

# def is_str_same(str1, str2):
#     print(f"{str1}\n{str2}")
#     if (str1 == str2):
#         return True
#         # print("True")
#     else:
#         return False
#         # print("False")

# num = int(input("Pls gib num: "))
# string_one = input("Pls gib string: ")
# string_two = input("Pls gib another string: ")
# is_num_five()
# is_str_same(string_one, string_two)

## attempt 1 at practice python 11
#
# def get_num():
#     return int(input("Gib num: "))

# def is_num_prime():
#     if (num > 1):
#         for i in range(2,num):
#             if (num % i) == 0:
#                 print(num,"is not prime.")
#                 # break
#             else:
#                 print(num,"is prime.")
#     else:
#         print(num,"is not prime.")

# num = get_num()
# is_num_prime()
# print(get_num)
# print(num)