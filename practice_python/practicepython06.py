#!/usr/bin/env python3
###
# Practice Python 6: string lists
# ###


def is_palindrome(string):
    if string[::1] == string[::-1]:
        return True
    else:
        return False

print(is_palindrome(input("Type a word and find out if it is a palindrome: ")))

# create a function that takes a string and tells me if that string is a palindrome or not
# pass string, get boolean